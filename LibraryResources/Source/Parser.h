#include <sstream>

class Parser {
	bool good;

public:
	double parseReal(const char *s) {
		std::istringstream str(s);
		mma::disownString(s);
		double res;
		str >> res;
		if (str.fail() || ! str.eof()) {
			good = false;
			return 0;
		}
		good = true;
		return res;
	}

	mint parseInteger(const char *s) {
		std::istringstream str(s);
		mma::disownString(s);
		mint res;
		str >> res;
		if (str.fail() || ! str.eof()) {
			good = false;
			return 0;
		}
		good = true;
		return res;
	}

	bool success() const { return good; }
};