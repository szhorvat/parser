(* Mathematica Package *)
(* Created by Mathematica plugin for IntelliJ IDEA *)

(* :Title: Parser *)
(* :Context: Parser` *)
(* :Author: szhorvat *)
(* :Date: 2015-09-22 *)

(* :Package Version: 0.1 *)
(* :Mathematica Version: *)
(* :Copyright: (c) 2015 szhorvat *)
(* :Keywords: *)
(* :Discussion: *)

BeginPackage["Parser`"]

ClearAll["`*"];

Get["LTemplate`LTemplatePrivate`"]

`Developer`Recompile::usage = "Recompile[]";
AppendTo[$ContextPath, $Context <> "Developer`"];
ConfigureLTemplate["MessageSymbol" -> Parser]

Parser::usage = "" (* there should only be a single object of this type *)

Begin["`Private`"]

$systemID = $SystemID;
(* On OS X libraries use libc++ ABI since M10.4 and libstdc++ ABI up to M10.3.  We need separate binaries. *)
If[$OperatingSystem === "MacOSX", $systemID = $systemID <> If[$VersionNumber <= 10.3, "-libstdc++", "-libc++"]];
$packageDirectory  = DirectoryName[$InputFileName];
$libraryDirectory  = FileNameJoin[{$packageDirectory, "LibraryResources", $systemID}];
$sourceDirectory   = FileNameJoin[{$packageDirectory, "LibraryResources", "Source"}];

template = LClass[
  "Parser",
  {
    LFun["parseReal", {"UTF8String"}, Real],
    LFun["parseInteger", {"UTF8String"}, Integer],
    LFun["success", {}, True | False]
  }
];

(* Add $libraryDirectory to $LibraryPath in case package is not installed in Applications. *)
If[Not@MemberQ[$LibraryPath, $libraryDirectory],
  PrependTo[$LibraryPath, $libraryDirectory]
]

Recompile[] :=
    Module[{},
      If[Not@DirectoryQ[$libraryDirectory],
        CreateDirectory[$libraryDirectory]
      ];
      SetDirectory[$sourceDirectory];
      Quiet@UnloadTemplate[template];
      Parser =.;
      CompileTemplate[template,
        "ShellCommandFunction" -> Print, "ShellOutputFunction" -> Print,
        "TargetDirectory" -> $libraryDirectory
      ];
      ResetDirectory[];
      LoadParser[]
    ]


LoadParser[] :=
    Module[{},
      If[LoadTemplate[template] === $Failed,
        Return[$Failed]
      ];
      Parser = Make["Parser"];
    ]

packageAbort[] := (End[]; EndPackage[]; Abort[])

If[LoadParser[] === $Failed,
  Print[Style["Loading failed, trying to recompile ...", Red]];
  If[Recompile[] === $Failed
    ,
    Print[Style["Cannot load or compile library. \[FreakedSmiley] Aborting.", Red]];
    packageAbort[]
    ,
    Print[Style["Successfully compiled and loaded the library. \[HappySmiley]", Red]];
  ]
]

Parser::usage = With[{sym = Symbol[LClassContext[] <> "Parser"]}, sym::usage]

End[] (* `Private` *)

EndPackage[]